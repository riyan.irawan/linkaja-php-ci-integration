<?php
/**
 * LinkAja Integration Lib For PHP CI
 * @Rynx
 * v1.0.0
 * For usage visit
 * https://gitlab.com/riyan.irawan/linkaja-php-ci-integration
 */
class Linkaja_library{
    const CREDENTIAL_UNAME="your-username";
    const CREDENTIAL_PWD="your-password";
    const CREDENTIAL_KEY='your-key';
    const MERCHANT_ID="your-merchant-id";
    const API_BASE_URL="https://partner-dev.linkaja.com/";
    const API_URLS=[
        'GENERATE_APP_LINK'=>"applink/v1/create"
    ];
    const ENV_DEV="dev";
    const ENV_PROD="prod";
    const ENCRYPTION=[
        'MODE'=>'AES',
        'KEY_SIZE'=>'256',
        'ENCRYPT_MODE'=>'CBC'
    ];

    protected $executor_id=1; //default Admin user id e.q system generated
    protected $ci;
    protected $ua_mode="web";

    public function __construct($params){
        $this->executor_id=isset($params['executor_id']) && $params['executor_id']!=null?$params['executor_id']:$this->executor_id;
        $this->ua_mode=isset($params['ua_mode'])?$params['ua_mode']:$this->ua_mode;
        $this->ci=& get_instance();
    }

    private function check_constructor_params(){
        $is_valid=true;
        $is_valid=($this->executor_id!=null && $this->ua_mode!=null);
        return $is_valid;
    }


    private function curl_processor($url=null,$method="GET",$header=[],$return_type="raw",$data=""){
        if(!$this->check_constructor_params()) return ['exec_time'=>date('Y:m:d H:i:s'),'result'=>'You have to complete params defined in constructor!'];
        $ch = curl_init(); 
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        /**
         * Disabled by default cause harcoded header auth.
         */
        // curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
        // curl_setopt($ch, CURLOPT_USERPWD,self::CREDENTIAL_UNAME.":".self::CREDENTIAL_PWD);
        if(strtolower($method)=="post"){
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        }
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        $output = curl_exec($ch); 
        curl_close($ch);

        /**
         * implement net i/o log system
         */
        $header_send=$header;
        $data_raw_send=$this->decryptor($data,str_replace('timestamp:','',$header[1]));
        $data_encrypt_send=$data;
        $this->write_log($url,$method,$header_send,$data_raw_send,$data_encrypt_send,json_decode($output));
        
        switch ($return_type) {
            case 'array':
                return ['exec_time'=>date('Y:m:d H:i:s'),'result'=>json_decode($output,TRUE)];
                break;
            case 'object':
                return ['exec_time'=>date('Y:m:d H:i:s'),'result'=>json_decode($output,FALSE)];
                break;
            default:
                return ['exec_time'=>date('Y:m:d H:i:s'),'result'=>$output];
                break;
        }
    }

    private function write_log($linkaja_url,$linkaja_method,$header_send,$data_raw_send,$data_encrypt_send,$data_return){
        
        $this->ci->load->library('user_agent');
        if ($this->ci->agent->is_browser())
        {
                $agent = $this->ci->agent->browser().' '.$this->ci->agent->version();
        }
        elseif ($this->ci->agent->is_robot())
        {
                $agent = $this->ci->agent->robot();
        }
        elseif ($this->ci->agent->is_mobile())
        {
                $agent = $this->ci->agent->mobile();
        }
        else
        {
                $agent = 'Unidentified User Agent';
        }
        $agent_platform=$this->ci->agent->platform()!=null?$this->ci->agent->platform():'Undefined Platform';
        $agent_str=$this->ci->agent->agent_string();

        $client_ip_address=$this->ci->input->ip_address();

        $log_data=[
            'app_url'=>current_url(),
            'app_http_method'=>$this->ci->input->method(FALSE),
            'linkaja_url'=>$linkaja_url,
            'linkaja_http_method'=>$linkaja_method,
            'header_send'=>json_encode($header_send,JSON_UNESCAPED_SLASHES),
            'data_raw_send'=>$data_raw_send,
            'data_encrypt_send'=>$data_encrypt_send,
            'data_return'=>json_encode($data_return,JSON_UNESCAPED_SLASHES),
            'ip'=>$client_ip_address,
            'executor_id'=>$this->executor_id,
            'user_agent_type'=>$agent,
            'user_agent_platform'=>$agent_platform,
            'user_agent_str'=>$agent_str,
            'exec_time'=>date('Y-m-d H:i:s'),
            'timezone'=>date_default_timezone_get()
        ];

        $this->ci->db->insert('linkaja_log',$log_data);
    }

    private function generate_header($timestamp="0000000000000",$ua="web",$contentType="application/json"){
        $auth='Basic '.base64_encode(self::CREDENTIAL_UNAME.":".self::CREDENTIAL_PWD);    
        $DEFAULT_HEADER=[
            "Content-Type:".$contentType,
            "timestamp:".$timestamp,
            "User-Agent:".$ua,
            "Authentication: ".$auth
        ];
        return $DEFAULT_HEADER;
    }

    private function generate_aes_iv(){
        $encryption_mode=self::ENCRYPTION['MODE'].'-'.self::ENCRYPTION['KEY_SIZE'].'-'.self::ENCRYPTION['ENCRYPT_MODE'];
        $ivlen = openssl_cipher_iv_length($encryption_mode);
        // return openssl_random_pseudo_bytes($ivlen);
        return strtotime(date('Y-m-d H:i:s')).'000000'; 
    }

    public function encryptor($data,$iv){
        $encryption_mode=self::ENCRYPTION['MODE'].'-'.self::ENCRYPTION['KEY_SIZE'].'-'.self::ENCRYPTION['ENCRYPT_MODE'];
        return base64_encode(openssl_encrypt($data, $encryption_mode , self::CREDENTIAL_KEY, OPENSSL_RAW_DATA, $iv));
    }
    public function decryptor($enctrypted_str,$iv){
        $encryption_mode=self::ENCRYPTION['MODE'].'-'.self::ENCRYPTION['KEY_SIZE'].'-'.self::ENCRYPTION['ENCRYPT_MODE'];
        return openssl_decrypt(base64_decode($enctrypted_str), $encryption_mode,self::CREDENTIAL_KEY , OPENSSL_RAW_DATA, $iv);
    }

    public function handshake(){
        $data=$this->curl_processor(self::API_BASE_URL.self::API_URLS['GENERATE_APP_LINK'],"GET",[],"object");
        return [
            'success'=>isset($data['result']->status)
        ];
    }

    public function create_app_link($trxDate
    ,$partnerTrxID
    ,$merchantID
    ,$terminalID
    ,$totalAmount
    ,$terminalName=null
    ,$partnerApplink=null
    ,$items=[]
    )
    {
        $body_params=[
            'trxDate'=>$trxDate,
            'partnerTrxID'=>$partnerTrxID,
            'merchantID'=>$merchantID,
            'terminalID'=>$terminalID,
            'terminalName'=>$terminalName,
            'totalAmount'=>$totalAmount,
            'partnerApplink'=>$partnerApplink,
            'items'=>$items
        ];

        $iv=$this->generate_aes_iv();
        $header=$this->generate_header($iv);
        $data=$this->encryptor(json_encode($body_params,JSON_UNESCAPED_SLASHES),$iv);
        return [
            'header'=>$header,
            'timestamp'=>$iv,
            'data_sended'=>$data,
            'linkaja_response'=>$this->curl_processor(self::API_BASE_URL.self::API_URLS['GENERATE_APP_LINK'],"POST",$header,"array",$data)
        ];

    }

    public function create_app_link_raw($body_params=[]){
        $iv=$this->generate_aes_iv();
        $header=$this->generate_header($iv);
        $data=$this->encryptor(json_encode($body_params,JSON_UNESCAPED_SLASHES),$iv);
        $linkaja_response=$this->curl_processor(self::API_BASE_URL.self::API_URLS['GENERATE_APP_LINK'],"POST",$header,"array",$data);

        return [
            'timestamp'=>$iv,
            'data_sended'=>$data,
            'linkaja_response'=>$linkaja_response
        ];

    }

    public function inform_payment(){
        $headers = $this->ci->input->request_headers();
        $body=$this->ci->input->raw_input_stream;
        $is_auth_valid=false;
        $is_header_complete=false;
        $is_header_complete=isset($headers['Authorization']) && isset($headers['Timestamp']);
        $is_header_auth_exist=isset($headers['Authorization']);
        $auth=[];
        $raw_data=[
            'headers'=>$headers,
            'body'=>null,
            'decrypted_body'=>null
        ];
        $timestamp="0000000000000000";
        if(isset($headers['Timestamp'])){
            $timestamp=(strlen($headers['Timestamp'])==16)?$headers['Timestamp']:$timestamp;
            // $timestamp=$this->generate_aes_iv();
        }

        //verify auth
        if($is_header_auth_exist){
            $auth=$headers['Authorization'];
            $auth=explode(' ',$auth);
        }
        if(count($auth)>0){
            $credentials=explode(':',base64_decode($auth[1]));
            if(count($credentials)>0){
                if($credentials[0]==self::CREDENTIAL_UNAME && $credentials[1]==self::CREDENTIAL_PWD){
                    $is_auth_valid=true;
                }
            }
        }


        //processing body
        if($is_auth_valid){
            if($is_header_complete && $body!=null){
                $raw_data['body']=$body;
                $decrypt_body=$this->decryptor($body,$timestamp);
                if($decrypt_body!=null){
                    $json_data=json_decode($decrypt_body,true);
                    if($json_data && isset($json_data['partnerTrxID']) && isset($json_data['linkAjaRefnum'])){
                        $raw_data['decrypted_body']=$json_data;
                        $data=[
                            "status"=> "00",
                            "message"=> "success",
                            "partnerTrxID" => $json_data['partnerTrxID'],
                            "linkAjaRefnum" => $json_data['linkAjaRefnum']
                        ];
                    }
                    else{
                        $data=[
                            "status"=> "02",
                            "message"=> "Invalid transaction"
                        ];
                    }
                }
                else{
                    $data=[
                        "status"=> "01",
                        "message"=> "Parameter missing",
                    ];
                }
            }
            else{
                $data=[
                    "status"=> "01",
                    "message"=> "Parameter missing",
                ];
            }
        }
        else{
            $data=[
                'status'=>"03",
                'message'=>'Unauthorized'
            ];
        }

        return [
            'api_response'=>$data,
            'raw_data'=>$raw_data
        ]; 
    }
}